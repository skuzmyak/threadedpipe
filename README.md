# ThreadedPipe
Lockless background data processing. Inspired by [Lockless Multi-Threading in Delphi](https://chapmanworld.com/2018/02/09/lockless-multi-threading-in-delphi/). 


Multiple data producers can push data to instance of `TInputMessagePipe<TIn>`, and multiple data consumers can subscribe to instance of `TOutputMessagePipe<TOut>`. Data processing happens in `TMessageHandler<TIn, TOut>` thread;

## Usage
Add `uThreadedPipe.pas` to your project. 


