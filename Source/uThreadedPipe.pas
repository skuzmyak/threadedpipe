unit uThreadedPipe;

interface

uses
  System.Classes,
  System.SyncObjs,
  System.Generics.Collections;

type
  TNotifyEvent<T> = procedure(Sender: TObject; const Data: T) of object;
  INotifyEvent<T> = interface;
  TNotifyEvents<T> = class;
  TMessageDispatcher<T> = class;

  /// <summary>
  ///   Base class for the input/output pipe
  ///   which incapsulates threaded queue.
  ///   Could be used by multiple producers/consumers, i.e. threads
  /// </summary>
  TMessagePipe<T> = class abstract(TObject)
  const
    cPipeCapacity = 10;
    cReceiveResponceTimeout = 10;
    cSendRequestTimeout = 100;
  strict private
    FInputEvent: TEvent;
    FItems: TThreadedQueue<T>;
    FEmptyEvent: TEvent;
    function GetCount: Cardinal;
  protected
    function InternalPull(out Data: T): Boolean; virtual;
    function InternalPush(const Data: T): Boolean; virtual;
    function WaitForInput: Boolean; virtual;
  public
    constructor Create(PipeCapacity: Cardinal = cPipeCapacity);
    destructor Destroy; override;

    /// <summary>
    ///   Event, which will be signaled when pipe becomes empty
    /// </summary>
    property EmptyEvent: TEvent read FEmptyEvent;
    property Count: Cardinal read GetCount;
  end;

  /// <summary>
  ///   Input message pipe, which could be used by
  ///  any number of producers, i.e. threads
  /// </summary>
  TInputMessagePipe<T> = class(TMessagePipe<T>)
  public
    function SendRequest(const Data: T): Boolean;
  end;

  /// <summary>
  ///   Base class of output pipe, which can be listened by any
  ///  number of consumers (thread) and additionaly by Main thread
  /// </summary>
  TOutputMessagePipe<T> = class abstract(TMessagePipe<T>)
  strict private
    FListeners: TNotifyEvents<T>;
  strict protected
    procedure InvokeEvents(const Data: T);
  public
    destructor Destroy; override;
    procedure AfterConstruction; override;

    procedure Subscribe(const Event: INotifyEvent<T>); overload;
    procedure Unsubscribe(const Event: INotifyEvent<T>); overload;
  end;

  /// <summary>
  ///   "Direct" output pipe, which will broadcast messages
  ///  as soon as they are received
  /// </summary>
  TOutputMessagePipeDirect<T> = class(TOutputMessagePipe<T>)
  protected
    function InternalPush(const Data: T): Boolean; override;
  end;

  /// <summary>
  ///   Message pipe, containing monitor thread,
  ///   which listens to the input messages and broadcasts them to event
  /// </summary>
  TOutputMessagePipeMonitored<T> = class(TOutputMessagePipe<T>)
  strict private
    FListener: TMessageDispatcher<T>;
    procedure DispatchMessageEvent(Sender: TObject; const Data: T);
  public
    destructor Destroy; override;
    procedure AfterConstruction; override;
  end;

  /// <summary>
  ///   Simple listener thread, which waits for the input to the pipe.
  ///  Could be also considered as output-less background worker
  /// </summary>
  TMessageDispatcher<T> = class(TThread)
  strict private
    FOnDispatch: TNotifyEvent<T>;
    FPipe: TMessagePipe<T>;
  strict protected
    procedure DispatchMessage(const Data: T); virtual;
    procedure DoExecute; virtual;
  protected
    procedure Execute; override;
    procedure TerminatedSet; override;
  public
    constructor Create(Pipe: TMessagePipe<T>);
    destructor Destroy; override;
    property OnDispatch: TNotifyEvent<T> read FOnDispatch write FOnDispatch;
  end;

  /// <summary>
  ///   Base worker class, which processes messages from the pipe
  ///   Could work only as background worker (when Output pipe = nil)
  ///   or additionally aggregate the resut of processed message to Output pipe
  /// </summary>
  TMessageHandler<TIn, TOut> = class abstract(TMessageDispatcher<TIn>)
  strict private
    FOutputPipe: TMessagePipe<TOut>;
  strict protected
    procedure DispatchMessage(const Data: TIn); override;
    /// <summary>
    ///   Actuall work which should be done according to the input message
    ///  Must be overriden in derived class
    /// </summary>
    function HandleMessage(const Input: TIn; out Output: TOut): Boolean; virtual;
  public
    constructor Create(InputPipe: TMessagePipe<TIn>; OutputPipe: TMessagePipe<TOut> = nil);
  end;

  TMessageHandlerFunc<TIn, TOut> = reference to function(const Input: TIn; out Output: TOut): Boolean;

  /// <summary>
  ///   Worker wich will execute delegated method within thread
  /// </summary>
  TDelegatedMessageProcessor<TIn, TOut> = class(TMessageHandler<TIn, TOut>)
  strict private
    FMessageHandler: TMessageHandlerFunc<TIn, TOut>;
  strict protected
    function HandleMessage(const Input: TIn; out Output: TOut): Boolean; override;
  public
    constructor Create(const MessageHandler: TMessageHandlerFunc<TIn, TOut>;
      InputPipe: TMessagePipe<TIn>; OutputPipe: TMessagePipe<TOut> = nil);
  end;

  INotifyEvent<T> = interface
  ['{B90D0584-BB6F-4D87-9556-229E211B285E}']
    function GetSynchronized: Boolean;
    function GetEnabled: Boolean;

    procedure OnChange(Sender: TObject; const Data: T);
    property Synchronized: Boolean read GetSynchronized;
    property Enabled: Boolean read GetEnabled;
  end;

  TNotifyEvents<T> = class(TThreadList<INotifyEvent<T>>)
  public
    procedure Invoke(Sender: TObject; const Data: T);
  end;

  TDelegatedNotifyEvent<T> = class(TInterfacedObject, INotifyEvent<T>)
  strict private //INotifyEvent<T>
    function GetSynchronized: Boolean;
    function GetEnabled: Boolean;
    procedure OnChange(Sender: TObject; const Data: T);
  strict protected
    FEnabled: Boolean;
    FEvent: TNotifyEvent<T>;
    FSync: Boolean;
    procedure SetEnabled(const Value: Boolean);
  public
    constructor Create(const Event: TNotifyEvent<T>; Synchronized: Boolean);
    property Enabled: Boolean read GetEnabled write SetEnabled;
  end;

  TDelegatedNotifyEventEx<T> = class(TDelegatedNotifyEvent<T>)
  public
    property Event: TNotifyEvent<T> read FEvent;
  end;

implementation

uses
  System.SysUtils,
  System.Generics.Defaults;

{ TMessageDispatcher<T> }

constructor TMessageDispatcher<T>.Create(Pipe: TMessagePipe<T>);
begin
  if not Assigned(Pipe) then
    raise EArgumentNilException.Create('Pipe must not be nil');
  inherited Create(False);
  FPipe := Pipe;
end;

destructor TMessageDispatcher<T>.Destroy;
begin
  FreeAndNil(FPipe);
  inherited;
end;

procedure TMessageDispatcher<T>.DispatchMessage(const Data: T);
begin
  if Assigned(FOnDispatch) then
    FOnDispatch(Self, Data);
end;

procedure TMessageDispatcher<T>.DoExecute;
var
  Response: T;
begin
  while FPipe.WaitForInput do
  begin
    if Self.Terminated then
      Break;

    while FPipe.InternalPull(Response) do
      DispatchMessage(Response);
  end;
end;

procedure TMessageDispatcher<T>.Execute;
begin
  NameThreadForDebugging('DISPATCHER_THREAD_' + IntToStr(Self.ThreadID));
  try
    DoExecute;
  except
    {TODO -oOwner -cGeneral : Handle exception}
  end;
end;

procedure TMessageDispatcher<T>.TerminatedSet;
begin
  inherited;
  //this will stop while loop
  FPipe.InternalPush(Default(T));
end;

procedure TNotifyEvents<T>.Invoke(Sender: TObject; const Data: T);
var
  Events: TList<INotifyEvent<T>>;
  Event: INotifyEvent<T>;
  i: Integer;
begin
  Events := Self.LockList;
  try
    for i := 0 to Events.Count - 1 do
    begin
      Event := Events.Items[i];
      if not Event.Enabled then
        Continue;

      if Event.Synchronized and (TThread.Current.ThreadID <> MainThreadID) then
      begin
        TThread.Queue(
          nil,
          procedure
          begin
            Event.OnChange(Sender, Data);
          end
        )
      end
      else
        Event.OnChange(Sender, Data);
    end;
  finally
    Self.UnlockList;
  end;
end;

{ TMessagePipe<T> }

constructor TMessagePipe<T>.Create(PipeCapacity: Cardinal = cPipeCapacity);
begin
  inherited Create;
  FItems := TThreadedQueue<T>.Create(PipeCapacity, cSendRequestTimeout, cReceiveResponceTimeout);
  FInputEvent := TEvent.Create;
  FEmptyEvent := TEvent.Create;
end;

destructor TMessagePipe<T>.Destroy;
begin
  FreeAndNil(FItems);
  FreeAndNil(FInputEvent);
  inherited;
end;

function TMessagePipe<T>.GetCount: Cardinal;
begin
  Result := FItems.QueueSize;
end;

function TMessagePipe<T>.InternalPull(out Data: T): Boolean;
begin
  Result := FItems.PopItem(Data) = wrSignaled;
  if Result and (FItems.QueueSize = 0) then
    FEmptyEvent.SetEvent;
end;

function TMessagePipe<T>.InternalPush(const Data: T): Boolean;
var
  Delta: Integer;
begin
  Result := FItems.PushItem(Data) = wrSignaled;
  if Result then
    FEmptyEvent.ResetEvent
  else
  begin
    Delta := FItems.TotalItemsPushed - FItems.TotalItemsPopped;
    if Delta >= FItems.QueueSize then
      raise EInvalidOpException.Create('Pipe overflow - pipe overloaded with messages');
  end;

  FInputEvent.SetEvent;
end;

function TMessagePipe<T>.WaitForInput: Boolean;
begin
  Result := FInputEvent.WaitFor = wrSignaled;
  FInputEvent.ResetEvent;
end;

{ TOutputMessagePipe<T> }

destructor TOutputMessagePipe<T>.Destroy;
begin
  FreeAndNil(FListeners);
  inherited;
end;

procedure TOutputMessagePipe<T>.InvokeEvents(const Data: T);
begin
  FListeners.Invoke(Self, Data);
end;

procedure TOutputMessagePipe<T>.Subscribe(const Event: INotifyEvent<T>);
begin
  FListeners.Add(Event);
end;

procedure TOutputMessagePipe<T>.AfterConstruction;
begin
  inherited;
  FListeners := TNotifyEvents<T>.Create;
end;

procedure TOutputMessagePipe<T>.Unsubscribe(const Event: INotifyEvent<T>);
begin
  FListeners.Remove(Event);
end;

{ TInputMessagePipe<T> }

function TInputMessagePipe<T>.SendRequest(const Data: T): Boolean;
begin
  Result := Self.InternalPush(Data);
end;

{ TMessageProcessor<TIn, TOut> }

constructor TMessageHandler<TIn, TOut>.Create(InputPipe: TMessagePipe<TIn>;
  OutputPipe: TMessagePipe<TOut>);
begin
  inherited Create(InputPipe);
  FOutputPipe := OutputPipe;
end;

procedure TMessageHandler<TIn, TOut>.DispatchMessage(const Data: TIn);
var
  Responce: TOut;
begin
  inherited DispatchMessage(Data);
  if HandleMessage(Data, Responce) and Assigned(FOutputPipe) then
    FOutputPipe.InternalPush(Responce);
end;

function TMessageHandler<TIn, TOut>.HandleMessage(const Input: TIn; out
  Output: TOut): Boolean;
begin
  raise ENotImplemented.Create('Process message method must be explicitly implemented in derived class');
end;

{ TDelegatedMessageProcessor<TIn, TOut> }

constructor TDelegatedMessageProcessor<TIn, TOut>.Create(const MessageHandler:
  TMessageHandlerFunc<TIn, TOut>; InputPipe: TMessagePipe<TIn>; OutputPipe: TMessagePipe<TOut>);
begin
  if not Assigned(MessageHandler) then
    raise EArgumentNilException.Create('Message processor delegate must not be nil');

  inherited Create(InputPipe, OutputPipe);
  FMessageHandler := MessageHandler;
end;

function TDelegatedMessageProcessor<TIn, TOut>.HandleMessage(const Input: TIn;
  out Output: TOut): Boolean;
begin
  Result := FMessageHandler(Input, Output);
end;

destructor TOutputMessagePipeMonitored<T>.Destroy;
begin
  FListener.Terminate;
  FreeAndNil(FListener);
  inherited;
end;

{ TOutputMessagePipeMonitored<T> }

procedure TOutputMessagePipeMonitored<T>.AfterConstruction;
begin
  inherited;
  FListener := TMessageDispatcher<T>.Create(Self);
  FListener.OnDispatch := DispatchMessageEvent;
end;

procedure TOutputMessagePipeMonitored<T>.DispatchMessageEvent(Sender: TObject; const Data: T);
begin
  InvokeEvents(Data);
end;

{ TOutputMessagePipeDirect<T> }

function TOutputMessagePipeDirect<T>.InternalPush(const Data: T): Boolean;
var
  Responce: T;
begin
  Result := inherited InternalPush(Data);
  while InternalPull(Responce) do
    InvokeEvents(Responce)
end;

{ TDelegatedNotifyEvent<T> }

constructor TDelegatedNotifyEvent<T>.Create(const Event: TNotifyEvent<T>;
  Synchronized: Boolean);
begin
  if not Assigned(Event) then
    raise EArgumentNilException.Create('Delegate event must be assigned');
  FEvent := Event;
  FSync := Synchronized;
  FEnabled := True;
end;

function TDelegatedNotifyEvent<T>.GetEnabled: Boolean;
begin
  Result := FEnabled;
end;

function TDelegatedNotifyEvent<T>.GetSynchronized: Boolean;
begin
  Result := FSync;
end;

procedure TDelegatedNotifyEvent<T>.OnChange(Sender: TObject; const Data: T);
begin
  FEvent(Sender, Data);
end;

procedure TDelegatedNotifyEvent<T>.SetEnabled(const Value: Boolean);
begin
  FEnabled := Value;
end;

end.
