unit uThreadedPipe.Tests;

interface

uses
  uThreadedPipe;


implementation

uses
  DUnitX.TestFramework,
  System.Classes,
  System.SyncObjs, System.SysUtils, Winapi.Windows, uThreadedPipe.Tests.Common;

type
  [TestFixture]
  TMessageDispatcherTests = class
  private
  public
    [Test]
    procedure ConcatStrings;
  end;

{ TMessageDispatcherTests }

procedure TMessageDispatcherTests.ConcatStrings;

  function ProcessString(const Input: string; out Output: TArray<string>): Boolean;
  begin
    Sleep(1000);
    Result := Input <> EmptyStr;
    if Result then
      Output := Input.Split([',']);
  end;

const
  cThreadArraySize = 10;
var
  Producers: TArray<TProducer<string>>;
  Handles: array[0..cThreadArraySize - 1] of THandle;
  i: Integer;
  Input: TInputMessagePipe<string>;
  Output: TOutputMessagePipe<TArray<string>>;
  Event: TEvent;
  WorkerProc: TMessageHandlerFunc<string, TArray<string>>;
  Worker: TDelegatedMessageProcessor<string, TArray<string>>;
begin
//  TDUnitX.CurrentRunner.Log(TLogLevel.Information, 'Information');
//  Log('test start');
//  WorkerProc :=
//    function (const Input: string; out Output: TArray<string>): Boolean
//    begin
//      Sleep(3000);
//      Result := Input <> EmptyStr;
//      if Result then
//        Output := Input.Split([',']);
//    end;
//
//  Input := TInputMessagePipe<string>.Create();
//  Output := TOutputMessagePipeMonitored<TArray<string>>.Create;
//
////  Output.Subscribe(ResponseReceivedEvent, False);
//  Worker := TDelegatedMessageProcessor<string, TArray<string>>.Create(WorkerProc, Input, Output);
//  Event := TEvent.Create;
//  SetLength(Producers, cThreadArraySize);
//  for i := 0 to cThreadArraySize - 1 do
//  begin
//    Producers[i] := TProducer<string>.Create(Input, Event, );
//    Handles[i] := Producers[i].Handle;
//    Producers[i].Start;
//  end;
////  Handles[cThreadArraySize] := Worker.Handle;
//  if MainThreadID <> TThread.CurrentThread.ThreadID then
//    Sleep(0);
//  Event.SetEvent;
//
////  Producers[I].Handle
////  Sleep(5000);
//  for i := 0 to cThreadArraySize - 1 do
//  begin
//
//    Producers[i].Terminate;
//    Producers[i].WaitFor;
//    Producers[i].fREE;
//  end;
//
////  WaitForMultipleObjects(cThreadArraySize, @Handles, True, INFINITE);
//
//  while True do
//  begin
////     CheckSynchronize();
//
//    if (Input.Count = 0) and (Output.Count = 0) then
//      Break;
//  end;
//  Worker.Terminate;
//  Worker.WaitFor;
end;

initialization
  TDUnitX.RegisterTestFixture(TMessageDispatcherTests)

end.
