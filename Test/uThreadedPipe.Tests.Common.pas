unit uThreadedPipe.Tests.Common;

interface

uses
  System.Classes,
  System.SyncObjs,
  System.SysUtils,
  uThreadedPipe
  ;

type
  TProducer<T> = class(TThread)
  private
    const cWaitTimeout = 5 * 1000;
  private
    FPipe: TInputMessagePipe<T>;
    FWorkEvent: TEvent;
    FDataProducer: TFunc<T>;
  protected
    procedure Execute; override;
    function ProduceData: T; virtual;
  public
    constructor Create(IntputPipe: TInputMessagePipe<T>; WorkEvent: TEvent;
      const DataProducer: TFunc<T>);
    destructor Destroy; override;
  end;

  TJsonFileReader = class(TProducer<string>)
  protected
    function ProduceData: string; override;
  end;

  TConsumer<T> = class(TMessageDispatcher<T>)
  private
    FPipe: TOutputMessagePipe<T>;
    FDataConsumer: TProc<T>;
    FEvent: INotifyEvent<T>;
    procedure ResponseReceivedEvent(Sender: TObject; const Data: T);
  protected
    procedure ConsumeData(const Data: T); virtual;
  public
    constructor Create(Pipe: TOutputMessagePipe<T>; const DataConsumer: TProc<T>);
    destructor Destroy; override;
  end;

  TMessageHandlerDelagates = class
  public
  end;


const
  cDemoJsonDataFielName = 'DemoJsonData.json';

implementation

uses
  System.IOUtils;


{ TProducer<T> }

constructor TProducer<T>.Create(IntputPipe: TInputMessagePipe<T>; WorkEvent: TEvent; const
  DataProducer: TFunc<T>);
begin
  inherited Create(True);
  FDataProducer := DataProducer;
  FPipe := IntputPipe;
  FWorkEvent := WorkEvent;
  FWorkEvent.ResetEvent;
end;

destructor TProducer<T>.Destroy;
begin

  inherited;
end;

procedure TProducer<T>.Execute;
var
  i: Integer;
begin
  NameThreadForDebugging('DATA_PRODUCER_TO_PIPE' + IntToStr(Self.ThreadID));
  while not Terminated do
  begin
    if FWorkEvent.WaitFor(cWaitTimeout) = wrSignaled then
    begin
      if Terminated then
        Exit;

      FPipe.SendRequest(Self.ProduceData);
    end;

  end;

end;

function TProducer<T>.ProduceData: T;
begin
  if Assigned(FDataProducer) then
    Result := FDataProducer();
end;

{ TConsumer<T> }

procedure TConsumer<T>.ConsumeData(const Data: T);
begin
  if Assigned(FDataConsumer) then
    FDataConsumer(Data);
end;

constructor TConsumer<T>.Create(Pipe: TOutputMessagePipe<T>; const DataConsumer: TProc<T>);
begin
  inherited Create(Pipe);
  FPipe := Pipe;
  FEvent := TDelegatedNotifyEvent<T>.Create(ResponseReceivedEvent, False);
  FPipe.Subscribe(FEvent);
  FDataConsumer := DataConsumer;
end;

destructor TConsumer<T>.Destroy;
begin
  FPipe.Unsubscribe(FEvent);
  inherited;
end;

procedure TConsumer<T>.ResponseReceivedEvent(Sender: TObject; const Data: T);
begin

end;

{ TJsonFileReader }

function TJsonFileReader.ProduceData: string;
const
  cInputDir = 'TestData\Input';
var
  SourceFilePath, DestFilePath: string;
  Source, Dest: TFileStream;
begin
//  TFile.Exists()
  SourceFilePath :=  TPath.Combine(GetCurrentDir, cDemoJsonDataFielName);
  Source := TFile.OpenRead(SourceFilePath);
  try
    DestFilePath := TPath.GetFileNameWithoutExtension(cDemoJsonDataFielName) + '_' + IntToStr(Self.ThreadID) + '.json';
    DestFilePath := TPath.Combine(cInputDir, DestFilePath);
    DestFilePath := TPath.Combine(GetCurrentDir, DestFilePath);
    TDirectory.CreateDirectory(DestFilePath);
    Dest := TFile.Open(DestFilePath, TFileMode.fmOpenOrCreate);
    try
      Dest.Position := 0;
      Dest.CopyFrom(Source, Source.Size);
    finally
      Dest.Free;
    end;
  finally
    Source.Free;
  end;

  Result := DestFilePath;
end;

end.
